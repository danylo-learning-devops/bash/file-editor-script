#!/bin/bash
# File editor. Give user option to compress or delete file.
# Write information aboutdeleted files in to log.txt in same folder.

init(){
	path="/home/veneomin/ss/scripts tasks/bash/file-editor"
	cd "$path"

	files=$(find . -type f -size +100k)
	declare -a filesArray
	filesArray=($files)

	chekIsFilesFound
}

function chekIsFilesFound(){
	if [ "${#filesArray[@]}" -gt "0" ]
	then
		echo "We find ${#filesArray[@]} files greater then 100K"
		echo "What are you want to do with them?"
		echo
		workWithFiles
	else
		echo "No files greater then 100K find"
	fi
	
}


workWithFiles(){
	for (( i=0; i < ${#filesArray[@]}; i++ ))
	do
		echo "Current file name is: ${filesArray[i]}"
		echo "1. Delete file"
		echo "2. Compress file"
		echo "Type 1 or 2"
		read whatToDo
		reset
		if [ "$whatToDo" -eq "1" ]
		then
			echo "You typed $whatToDo"
			echo "File ${filesArray[i]} was successfuly deleted."
			#rm ${filesArray[i]}
			writeLogsToFile "${filesArray[i]}"
		elif [ "$whatToDo" -eq "2" ]
		then
			echo "You typed $whatToDo"
			echo "File ${filesArray[i]} was successfuly compressed."
			#gzip ${filesArray[i]}
		else
			echo "Incorrect input"
		fi
	done
}

writeLogsToFile(){
	logFile=$(find -name log.txt)

	if [ $logFile ]
	then 
		echo "$1" >> log.txt
		line=$(head -n 1 log.txt)

		getOldNumberOfDeletedFiles
		newCountOfDeletedItems=$(($oldCountOfDeletedFiles + 1))
		firstLineOfFile="Deleted files: "
		firstLineOfFile+=$newCountOfDeletedItems
		sed -i -e "1d" log.txt
		sed -i "1i $firstLineOfFile" log.txt
	else 
		touch log.txt
		echo "Deleted files: 1" >> log.txt
		echo "$1" >> log.txt
	fi
}

getOldNumberOfDeletedFiles(){
	line=$(head -n 1 log.txt)
	oldCountOfDeletedFiles=$(echo $line | grep -o -E '[0-9]+')
}

init